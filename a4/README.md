> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Web Application Development

## Landon Stefanick

### Assignment # 4 Requirements:



*Four Parts*

1. Course title, your name, assignment requirements
2. Screenshots of client-side valiadation
3. Chapter Questions (Chs 9, 10,19)
4. Link to local lis4381 web app: [http://localhost/lis4381](http://localhost/lis4381 "http://localhost/lis4381")

#### Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4381 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 





#### Assignment Screenshots:

*Lis4381 Portal (Main Page)*:

![Portal Screenshot](img/portal.png)

*Screenshot of failed validation*:

![Failed Validation](img/failed.png)

*Screenshot of passed validation - My First App*:

![Passed Validation](img/passed.png)



