> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Web Application Development

## Landon Stefanick

### Project # 1 Requirements:



*Two Parts*

1. Development My Business Card Application
2. Chapter Questions (Chs 7, 8)

#### README.md file should include the following items:

* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface


#### Assignment Screenshots:

* Screenshot of first application screen:

![Screen 1](img/first.png)

* Screenshot of second application screen:

![Screen 2](img/second.png)




