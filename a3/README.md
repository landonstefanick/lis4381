> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Web Application Development

## Landon Stefanick

### Assignment # 3 Requirements:



*Three Parts*

1. Create a database diagram.
2. Create a mobile application.
3. Chapter Questions (Chs 5, 6)

#### README.md file should include the following items:

* Screenshot of ERD.
* Screenshop of running applications first screen.
* Screenshot of running applications second screen.
* Links to the following files: 
    * a. a3.mwb
    * b. a3.sql





#### Assignment Screenshots:

* Screenshot of ERD:

![ERD Screenshot](img/a3.png)

* Screenshot of running applications first screen:

![First application screen](img/first.png)

* Screenshot of running applications second screen:

![Second application screen](img/second.png)



#### Links:

*A3 Workbench File:*
[A3 MWB](https://bitbucket.org/landonstefanick/lis4381/src/ca42f1ea731ac4836e06184b2a6a6aa9161144e0/a3/docs/a3.mwb?at=master&fileviewer=file-view-default "A3 workbench")

*A3 SQL File:*
[A3 SQL](https://bitbucket.org/landonstefanick/lis4381/src/78bbb02e505a7d265541b25ad551f76abd0d0dcf/a3/docs/a3.sql?at=master&fileviewer=file-view-default "A3 SQL")
