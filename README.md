# Lis 4381- Mobile Web Application Developement 

## Landon Stefanick

### LIS 4381

*Course Work Links*:


[A1 Subdirectory](a1/README.md "A1 Coursework")

* Screenshot of APSS Installation MY PHP Installation.
* Screenshot of running java Hello.
* Screenshot of running Android Studio - My First App
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)


[A2 Subdirectory](a2/README.md "A2 Coursework")

* Create App Healthy Recipe
* Screenshot of first application screen.
* Screenshot of second application screen.



[A3 Subdirectory](a3/README.md "A3 Coursework")

* Screenshot of ERD.
* Screenshop of running applications first screen.
* Screenshot of running applications second screen.
* Links to the following files: 
    * a. a3.mwb
    * b. a3.sql


[A4 Subdirectory](a4/README.md "A4 Coursework")

* Client-side validation
* Link to localhost
* Screenshot of main portal page
* Screenshot of passed and failed validation


[A5 Subdirectory](a5/README.md "A5 Coursework")

* Server-side validation
* Link to localhost
* Screenshot of index.php
* Screenshot of app_petstore_process.php


[P1 Subdirectory](p1/README.md "P1 Coursework")

* Create My Business Card App
* Screenshot of running application's first user interface screen.
* Screenshot of running application's second user interface screen.


[P2 Subdirectory](p2/README.md "P2 Coursework")

* Add edit functionality to A5
* Add delete functionality to A5
* Create an RSS feed page
