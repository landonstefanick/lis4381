> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis 4381 Mobile Web Application Development

## Landon Stefanick

### Project # 2 Requirements:



*Five Parts*

1. Add edit and delete funcationality to A5
2. Create a page with an RSS feed
3. Screenshots of project requirements
4. Chapter Questions (Chs 11, 12)
5. Link to local lis4381 web app: [http://localhost/lis4381](http://localhost/lis4381 "http://localhost/lis4381")

#### Deliverables:
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4381 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 





#### Project Screenshots:

*Project 2 Index.php*:

![Index.php Screenshot](img/index.png)

*edit_petstore.php*:

![edit.php](img/edit.png)

*edit_petstore_process.php that includes error.php*:

![Failed Validation](img/error.png)

*Carousel - Home Page*:

![Home Page](img/portal.png)

*RSS Feed*:

![RSS Feed](img/rssfeed.png)






